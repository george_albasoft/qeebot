# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

* Configure Rpi

* Setup DDNS  
   1. Route53  
      - Head over to the Hosted Zones portion of Route53.  
      - Copy the Hosted Zone ID somewhere. You will need it later.  
      - Create Record Set for subdomain to link with IP.  
      - For testing purposes, put 8.8.8.8 in this field.  
      - Once PynamicDNS starts working, we will come back here.  
      
   2. Create IAM role
      - User name should be pynamicdns.  
      - Check the Programmatic access box.  
      - Select Attach existing policies directly near the top.  
      - Check the box for AmazonRoute53FullAccess. Save.  
      - Save all info, Click the Download .csv button.  
      
   3. RaspberryPi  
  
      * sudo su -
      * apt-get update -y  
      * apt install python3-pip git -y  
      * python3 -m pip install requests==2.22.0  
      * pip3 install boto3  
      * boto3 will need to use the user we created in AWS IAM earlier.  
      * mkdir ~/.aws  
      * touch ~/.aws/credentials  
      ```
      [default]  
      aws_access_key_id = <Access key ID>   
      aws_secret_access_key = <Secret access key>  
      ```
      touch ~/.aws/config  
      ``` 
      [default]  
      region=us-west-2  
      ```
      ```
      git clone https://github.com/tynick/PynamicDNS.git  
      python3 ~/PynamicDNS/PynamicDNS.py <DNS Record> <Hosted Zone ID>  
      crontab -e  
      */5 * * * * python3 ~/PynamicDNS/PynamicDNS.py <DNS Record> <Hosted Zone ID>  
      ```
      
      
### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
